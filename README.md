# **RobotiKid Android Project: CSCI201 Spring 16' Group Project**
**Contributors:** Michael Bachmann, Jean Tu, Casey Klecan, Pawel Furtek and Shandira Ferguson

RobotiKid is an android project aimed at teaching kids from ages 5-7 basic programming principals in a fun and interactive way. The project is divided into two main sections the fronted android application and the backend server application that persists user data and verifies various programming challenges.

## **NOTE**
Do a `git pull` and it should grab all three branches, master, front_end and back_end. Make sure to run `git checkout front_end` or `git checkout back_end` when your'e working on stuff. Commits from there will be local to that branch and to push them run `git push origin front_end` or `git push origin back_end` to push the changes to the respective remote branches. 

## **Android Application**
* Current Build: 0.1
* Minimum Android Requirement: Lollipop
#### Instructions to build and run
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

## **Serverside Application**
* Current Build: 0.1
* Minimum Java Requirement: 1.8
#### Instructions to build and run
* Summary of set up
* Configuration
* Dependencies
    * [Jetty 9](http://www.eclipse.org/jetty/)
    * [Jersey 2.2](https://jersey.java.net)
    * [JJWT](https://github.com/jwtk/jjwt)
    * [MongoDB + Java Driver](https://www.mongodb.org)
* Database configuration
* How to run tests
* Deployment instructions

# **Global Models (Client and Server Side)**

## **User Model**
**_Note_**, certain variables are included in both classes, this is explicitly done without the use of polymorphism 
because Jackson (framework for serializing Java objects to JSON for HTTP requests) can only serialize POJO 
(Plain Old Java Object) and if a we implemented a User interface it would not work because POJO's cannot 
extend or implement another class/interface. _However_ in the database there is only one instance of each user 
type since users are stored as JSON object as we are using MongoDB. The need for certain types of users and the 
convenience of Jackson outweighed the benefits of parsing strings. **_Also note_**, constrictions on the member 
variables are placed adjacent to the variable name.

#### User
Users (objects not all their collective information) have the following properties.

* **username**: 5-25 characters, not case sensitive, only allows alphabetical characters and numbers
* **battery**: 0-9
* **email**: something@something.something
* **cur_level**: 0-5
* **cur_challenge**: 0-5
* **friends_list**: 0-100

#### Guest
Guests only have the following properties.

* **username**: 5-25 characters, not case sensitive, only allows alphabetical characters and numbers
* **battery**: 0-9
* **email**: something@something.something
* **cur_level**: 0-5
* **cur_challenge**: 0-5

#### Friend
Friends only have the following properties.

* **username**: 5-25 characters, not case sensitive, only allows alphabetical characters and numbers
* **battery**: 0-9
* **email**: something@something.something
* **cur_level**: 0-5
* **cur_challenge**: 0-5

#### Credentials
Explicitly only used before a token is obtained. Password should be 5-20 characters, case sensitive, 
types of characters allowed / not allowed? Security Answer should be 5-50 characters, NOT case 
sensitive, only allows alphabetical characters and numbers

* **username**: 5-25 characters, not case sensitive, only allows alphabetical characters and numbers
* **battery**: 0-9
* **email**: something@something.something
* **cur_level**: 0-5
* **cur_challenge**: 0-5
* **pass_hash**: 5-25, defined by hashing algorithm
* **security_ans_hash**: defined by hashing algorithm
* **security_question_num**: 0-2

## **Answer Model**
There is only one answer model and it is templated so as to do custom validation server side.

#### Answer
Answer's contain the following properties. **_note_** depending on the types of challenges we'll 
figure out the best way to constrict this.

* **level_number**: 0-5
* **challenge_number**: 0-5
* **is_correct**: true/false
* **answer**: 0 - ???? characters ????